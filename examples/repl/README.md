# Hoot REPL example

This example demonstrates a text-only REPL suitable for running in
either the Hoot Wasm interpreter or NodeJS.

## Try it out

Run on the Hoot Wasm interpreter:

```
make run-guile
```

Run on NodeJS:

```
make run-node
```
