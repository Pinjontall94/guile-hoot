;;; Copyright (C) 2025 Igalia, S.L.
;;; Copyright (C) 2024 David Thompson <dave@spritely.institute>
;;; Copyright (C) 2023, 2024 Robin Templeton
;;;
;;; Licensed under the Apache License, Version 2.0 (the "License");
;;; you may not use this file except in compliance with the License.
;;; You may obtain a copy of the License at
;;;
;;;    http://www.apache.org/licenses/LICENSE-2.0
;;;
;;; Unless required by applicable law or agreed to in writing, software
;;; distributed under the License is distributed on an "AS IS" BASIS,
;;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;;; See the License for the specific language governing permissions and
;;; limitations under the License.

;;; Commentary:
;;;
;;; Hashtable tests.
;;;
;;; Code:

(use-modules (srfi srfi-64)
             (test utils))

(test-begin "test-hash")

(with-additional-imports ((hoot hashtables))
  ;; Hashing numbers
  (test-call "6" hashq 42 37)

  ;; Hashing pairs and lists.
  (test-call "228" hash '(a . b) 389)
  (test-call "94" hash '(a b) 389)
  ;; Deeply nested list.
  (test-call "69" hash '(a (b (c (d (e (f (g (h (i))))))))) 389)
  ;; Circular list!
  (test-call "65" hash
             (let ((x (list 'a 'b 'c)))
               (set-cdr! (cdr (cdr x)) x)
               x)
             389)
  ;; Hash composition should not be commutative.
  (test-call "#f" (lambda () (= (hash '(a . b) 389) (hash '(b . a) 389))))

  ;; Hashing vectors of different length.
  (test-call "200" hash #() 389)
  (test-call "222" hash #(1 2 3) 389)

  ;; Hashing bytevectors of different length.
  (test-call "51"  hash #vu8() 389)
  (test-call "155" hash #vu8(1) 389)
  (test-call "224" hash #vu8(1 2) 389)
  (test-call "294" hash #vu8(1 2 3) 389)
  (test-call "206" hash #vu8(1 2 3 4) 389)

  ;; Hashing bitvectors of different length.
  (test-call "173" hash #* 389)
  (test-call "195" hash #*1010 389)
  (test-call "119" hash #*01010 389)

  ;; Empty bytevector should have different hash than empty bitvector.
  (test-call "#f" (lambda () (= (hash #vu8() 389) (hash #* 389))))

  ;; Hashing records.
  (test-call "222" hash
             (let ()
               (define-record-type q (make-q a) q? (a q-a))
               (make-q 42))
             389))

(test-end* "test-hash")
